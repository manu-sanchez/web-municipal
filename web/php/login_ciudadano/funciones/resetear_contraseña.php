<?php
 session_start();  
 if(!isset($_SESSION["username"]))  
 {  
      header("location:..\iniciarsesion.php?action=login");  
 }  
require_once "..\mysql\configuracion.php";

$new_password = $confirm_password = $username = $password = "";
$new_password_err = $confirm_password_err = $username_err = $password_err = "";
 
if($_SERVER["REQUEST_METHOD"] == "POST"){
    
       if(empty(trim($_POST["username"]))){
        $username_err = "Ponga su usuario.";
    } else{
        $username = trim($_POST["username"]);
    }
    if(empty(trim($_POST["password"]))){
        $password_err = "Ponga su contraseña.";
    } else{
        $password = trim($_POST["password"]);
    }
    if(empty(trim($_POST["new_password"]))){
        $new_password_err = "Ponga la nueva contraseña.";     
    } elseif(strlen(trim($_POST["new_password"])) < 6){
        $new_password_err = "Minimo 6 caracteres.";
    } else{
        $new_password = trim($_POST["new_password"]);
    }
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Confirma la contraseña.";
    } else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($new_password_err) && ($new_password != $confirm_password)){
            $confirm_password_err = "La contraseña no es igual.";
        }
    }
       
    if(empty($username_err) && empty($password_err)){
        $password = md5($password, PASSWORD_DEFAULT);
        $confirm_password=md5($confirm_password, PASSWORD_DEFAULT);
        $sql = "SELECT username, password FROM usuario_ciudadano WHERE username = '$username' AND password='$password'";
        $sqla = "UPDATE usuario_ciudadano SET password = '$confirm_password' WHERE username = '$username'";
        $resul = mysqli_query($link,$sql);
        if(mysqli_num_rows($resul)>0){
        if(mysqli_query($link, $sqla) ){
            header("location: salir.php");
                                 
                             }else{
                    echo "error no se puede aztualizar";
               }     
        }else{
               }   
    }else{
        echo "La contraseña o el usuario son incorrectos";
 }
    
        
    mysqli_close($link);
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Resetear contraseña</title>
    <link rel="stylesheet" href="">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body>
    <div class="wrapper">
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
         <h2>Resetear contraseña</h2>
        <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                <label>Usuario</label>
                <input type="text" name="username" class="form-control" value="<?php echo $username; ?>">
                <span class="help-block"><?php echo $username_err; ?></span>
            </div>    
            <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                <label>Antigua Contraseña</label>
                <input type="password" name="password" class="form-control">
                <span class="help-block"><?php echo $password_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($new_password_err)) ? 'has-error' : ''; ?>">
                <label>Nueva contraseña</label>
                <input type="password" name="new_password" class="form-control" value="<?php echo $new_password; ?>">
                <span class="help-block"><?php echo $new_password_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
                <label>Confirma contraseña</label>
                <input type="password" name="confirm_password" class="form-control">
                <span class="help-block"><?php echo $confirm_password_err; ?></span>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Cambiar">
                <a class="btn btn-link" href="..\bienvenido\bienvenido.php">Cancelar</a>
            </div>
        </form>
    </div>    
</body>
</html>