<?php
require_once "..\mysql\configuracion.php";
$username = $password = $confirm_password = $dni = $email =  $nombre = $apellido1 = $apellido2 = $postal = $direccion = "";
$username_err = $password_err = $confirm_password_err = $dni_err = $email_err = $nombre_err = $apellido1_err = $apellido2_err = $postal_err = $direccion_err = "";

if($_SERVER["REQUEST_METHOD"] == "POST"){
    if(empty(trim($_POST["username"]))){
        $username_err = "Ponga su contraseña.";
    } else{
        $sql = "SELECT id FROM usuario_ciudadano WHERE username = ?";
        
        if($stmt = mysqli_prepare($link, $sql)){
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            $param_username = trim($_POST["username"]);
            if(mysqli_stmt_execute($stmt)){
                mysqli_stmt_store_result($stmt);
                if(mysqli_stmt_num_rows($stmt) == 1){
                    $username_err = "El usuario ya esiste.";
                } else{
                    $username = trim($_POST["username"]);
                }
            } else{
                echo "ERROR! Intentelo mas tarde.";
            }
        }
        mysqli_stmt_close($stmt);
    }
    if(empty(trim($_POST["password"]))){
        $password_err = "ponga su contraseña.";     
    } elseif(strlen(trim($_POST["password"])) < 6){
        $password_err = "Minimo 6 caracteres.";
    } else{
        $password = trim($_POST["password"]);
    }
   if(empty(trim($_POST["nombre"]))){
        $nombre_err = "ponga su nombre.";     
    } elseif(strlen(trim($_POST["nombre"])) < 1){
        $nombre_err = "Minimo 7 caracteres.";
    } else{
        $nombre = trim($_POST["nombre"]);
    }
    if(empty(trim($_POST["apellido1"]))){
        $apellido1_err = "ponga su apellido.";     
    } elseif(strlen(trim($_POST["apellido1"])) < 1){
        $apellido1_err = "Minimo 7 caracteres.";
    } else{
        $apellido1 = trim($_POST["apellido1"]);
    }
    if(empty(trim($_POST["apellido2"]))){
        $apellido2_err = "ponga su apellido.";     
    } elseif(strlen(trim($_POST["apellido2"])) < 1){
        $apellido2_err = "Minimo 7 caracteres.";
    } else{
        $apellido2 = trim($_POST["apellido2"]);
    }
    if(empty(trim($_POST["dni"]))){
        $dni_err = "ponga su CIF/NIF.";     
    } elseif(strlen(trim($_POST["dni"])) < 7){
        $dni_err = "Minimo 7 caracteres.";
    } else{
        $dni = trim($_POST["dni"]);
    }  
    if(empty(trim($_POST["direccion"]))){
        $direccion_err = "ponga su direccion.";     
    } elseif(strlen(trim($_POST["direccion"])) < 7){
        $direccion_err = "Minimo 7 caracteres.";
    } else{
        $direccion = trim($_POST["direccion"]);
    }
    if(empty(trim($_POST["postal"]))){
        $postal_err = "ponga su postal.";     
    } elseif(strlen(trim($_POST["postal"])) < 4){
        $postal_err = "Minimo 4 caracteres.";
    } else{
        $postal = trim($_POST["postal"]);
    }
    if(empty(trim($_POST["email"]))){
        $email_err = "ponga su email.";     
    } elseif(strlen(trim($_POST["email"])) < 7){
        $email_err = "Minimo 7 caracteres.";
    } else{
        $email = trim($_POST["email"]);
    }
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Confirma tu contraseña.";     
    } else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($password_err) && ($password != $confirm_password)){
            $confirm_password_err = "La contraseña no es igual.";
        }
    }
    if(empty($username_err) && empty($password_err) && empty($confirm_password_err)){
        $sql = "INSERT INTO usuario_ciudadano (username, password, nombre, apellido1, apellido2, dni, direccion, postal, email, valido) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, 'true')";   
        $sqlvalues = "UPDATE usuario_ciudadano SET tipo = 'Ciudadano' where username ='$username'  ";
        if($stmt = mysqli_prepare($link, $sql)){
            mysqli_stmt_bind_param($stmt, "sssssssss", $param_username, $param_password, $param_nombre, $param_apellido1, $param_apellido2, $param_dni, $param_direccion, $param_postal, $param_email);
            $param_username = $username;
            $param_password = md5($password, PASSWORD_DEFAULT);
            $param_nombre = $nombre;
            $param_apellido1 = $apellido1;
            $param_apellido2 = $apellido2;
            $param_dni = $dni;
            $param_direccion = $direccion;
            $param_postal = $postal;
            $param_email = $email;
            if(mysqli_stmt_execute($stmt)){
                header("location: ..\iniciarsesion.php");
            } else{
                echo "ERROR! Intentelo mas tarde.";
            }
        }
        if(mysqli_query($link,$sqlvalues)){
        }else{
        }
        mysqli_stmt_close($stmt);
    }
    mysqli_close($link);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Resgistrarse</title>
    <link rel="stylesheet" href="">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body>
    <div class="wrapper">
        <h2>Resgistrarse</h2>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                <label>Usuario</label>
                <input type="text" name="username" class="form-control" value="<?php echo $username; ?>">
                <span class="help-block"><?php echo $username_err; ?></span>
            </div>    
            <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                <label>Contraseña</label>
                <input type="password" name="password" class="form-control" value="<?php echo $password; ?>">
                <span class="help-block"><?php echo $password_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
                <label>Confirm Password</label>
                <input type="password" name="confirm_password" class="form-control" value="<?php echo $confirm_password; ?>">
                <span class="help-block"><?php echo $confirm_password_err; ?></span>
            </div>
             <div class="form-group <?php echo (!empty($nombre_err)) ? 'has-error' : ''; ?>">
                <label>Nombre</label>
                <input type="text" name="nombre" class="form-control" value="">
                <span class="help-block"><?php echo $nombre_err; ?></span>
            </div> 
             <div class="form-group <?php echo (!empty($apellido1_err)) ? 'has-error' : ''; ?>">
                <label>Primer Apellido</label>
                <input type="text" name="apellido1" class="form-control" value="">
                <span class="help-block"><?php echo $apellido1_err; ?></span>
            </div> 
            <div class="form-group <?php echo (!empty($apellido2_err)) ? 'has-error' : ''; ?>">
                <label>Segundo Apellido</label>
                <input type="text" name="apellido2" class="form-control" value="">
                <span class="help-block"><?php echo $apellido2_err; ?></span>
            </div> 
            <div class="form-group <?php echo (!empty($dni_err)) ? 'has-error' : ''; ?>">
                <label>CIF/NIF</label>
                <input type="text" name="dni" class="form-control" value="">
                <span class="help-block"><?php echo $dni_err; ?></span>
            </div> 
            <div class="form-group <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>">
                <label>Correo Electronico</label>
                <input type="text" name="email" class="form-control" value="">
                <span class="help-block"><?php echo $email_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($direccion_err)) ? 'has-error' : ''; ?>">
                <label>Dirección</label>
                <input type="text" name="direccion" class="form-control" value="">
                <span class="help-block"><?php echo $direccion_err; ?></span>
            </div> 
            <div class="form-group <?php echo (!empty($postal_err)) ? 'has-error' : ''; ?>">
                <label>Codigo Postal</label>
                <input type="text" name="postal" class="form-control" value="">
                <span class="help-block"><?php echo $postal_err; ?></span>
            </div>  
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Enviar">
                <input type="reset" class="btn btn-default" value="Borrar">
            </div>
            <p>Tienes cuenta? <a href="..\iniciarsesion.php">Inicia sesion</a>.</p>
        </form>
    </div>    
</body>
</html>