<?php
include('mysql\configuracion.php');
session_start();
if(isset($_SESSION["username"]))  
 {  
      header("location:bienvenido\bienvenido.php");  
 }  
$username = $password = "";
$username_err = $password_err = "";
if($_SERVER['REQUEST_METHOD'] == "POST"){
if(empty(trim($_POST["username"]))){
        $username_err = "Ponga su usuario.";
    } else{
        $username = trim($_POST["username"]);
    }
    if(empty(trim($_POST["password"]))){
        $password_err = "Ponga su contraseña.";
    } else{
        $password = trim($_POST["password"]);
    }
    if(empty($username_err) && empty($password_err)){
    $password = md5($password, PASSWORD_DEFAULT);
    $sql = "SELECT * FROM usuario_trabajadores WHERE username='$username' AND password='$password'"; 
        $sqladministrativo = "SELECT * FROM usuario_trabajadores WHERE username='$username' AND tipo = 'Administracion' ";
        $sqlentidad = "SELECT * FROM usuario_trabajadores WHERE username='$username' AND tipo = 'Entidad' AND valido = 'true'  ";
        $sqlfuncionario = "SELECT * FROM usuario_trabajadores WHERE username='$username' AND tipo = 'Funcionario' AND valido = 'true'  ";
        $sqlpolicia = "SELECT * FROM usuario_trabajadores WHERE username='$username' AND tipo = 'Policia' AND valido = 'true' ";
        $sqlespere = "SELECT * FROM usuario_trabajadores WHERE username='$username' AND valido='false' ";
 if(mysqli_num_rows(mysqli_query($link,$sql))>0){
     $_SESSION['username'] = $username; 
     if( mysqli_num_rows(mysqli_query($link,$sqlespere)) == 1 ){
                                 header("location: bienvenido\bienvenidoespere.php");
                             }else{
                            if( mysqli_num_rows(mysqli_query($link,$sqladministrativo)) == 1 ){
                                header("location: bienvenido\bienvenidoadministrativo.php");
                            }else{
                                    if( mysqli_num_rows(mysqli_query($link,$sqlentidad)) == 1 ){
                                        header("location: bienvenido\bienvenidoentidad.php");
                                    }else{
                                        if( mysqli_num_rows(mysqli_query($link,$sqlfuncionario)) == 1 ){
                                            header("location: bienvenido\bienvenidofuncionario.php");
                                        }else{
                                            if( mysqli_num_rows(mysqli_query($link,$sqlpolicia)) == 1 ){
                                                header("location: bienvenido\bienvenidopolicia.php");
                                            }else{
                                            
                                          }
                                       }
                                    }
                                
                            }               
                            
                        } 
     
 }else{
 echo "La contraseña o el usuario son incorrectos";
 }
    }
}

    
    
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Iniciar Sesion</title>
    <link rel="stylesheet" type="text/css" href="../csslogin/estilo-login.css">
    
</head>
<body>
    <header>
        <h1 class="ayto"><a href="../../index.html">Ayuntamiento de Boolean</a></h1>
        <h1 class="titulo">Sede electrónica</h1>
    </header>
    <div class="wrapper">
        <h2>Iniciar Sesion</h2>
        <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
            <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                <label>Usuario</label>
                <input type="text" name="username" class="form-control" value="<?php echo $username; ?>">
                <span class="help-block"><?php echo $username_err; ?></span>
            </div>    
            <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                <label>Contraseña</label>
                <input type="password" name="password" class="form-control">
                <span class="help-block"><?php echo $password_err; ?></span>
            </div>
            <div class="form-group" id="boton">
                <br />  
                     <button type="submit" name="login" class="btn btn-info">aceptar</button>   
                <br /> 
            </div>
            <p>No tienes cuenta? <a href="funciones\registrar.php">Creala</a>.</p>
        </form>
    </div>    
</body>
</html>