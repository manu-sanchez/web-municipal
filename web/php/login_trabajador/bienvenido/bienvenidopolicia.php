<?php
  session_start();  
 if(!isset($_SESSION["username"]))  
 {  
      header("location:../iniciarsesion.php?action=login");  
 }  
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Bienvenido</title>
    <link rel="stylesheet" href="">
    <style type="text/css">
        body{ font: 14px sans-serif; text-align: center; }
    </style>
</head>
<body>
    <div class="page-header">
        <h1>Hola, <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b>. Bienvenido a la pagina. Su rol es Policia.</h1>
    </div>
    <p>
        <a href="..\funciones\resetear_contraseña.php" class="btn btn-warning">Cambiar contraseña</a>
        <a href="..\funciones\salir.php" class="btn btn-danger">Cerrar sesion</a>
    </p>
</body>
</html>