<?php



//COMPROBACIÓN AÑO BISIESTO
function esbisiesto($anno) {
    
    if ( ($anno%4==0 AND $anno%100!=0) OR $anno%400==0 ){
        return true;
    } else {
        return false;
    } 
    
}


//COMPROBACIÓN DE FECHA DADA ES CORRECTA
function fechaOK ($fechanac){

   $anno=$fechanac[0].$fechanac[1].$fechanac[2].$fechanac[3];
   $mes=$fechanac[5].$fechanac[6];
   $dia=$fechanac[8].$fechanac[9];
    
   if( $mes>=1 AND $mes<=12 AND $anno>=0 AND $dia>=1 ){
    
        if ( ($mes==1 OR $mes==3 OR $mes==5 OR $mes==7 OR $mes==8 OR $mes==10 OR $mes==12) AND $dia<=31 ) {
            return true;
        }   
        elseif ( ($mes==4 OR $mes==6 OR $mes==9 OR $mes==11) AND $dia<=30 ) {
            return true;
        }
        elseif ( ($mes == 2 AND $dia<=28) OR ($mes == 2 AND $dia<=29 AND esbisiesto($anno)) ) {
            return true;
        }   
        else{
            return false;
        }
    
    }   else{  
        return false;
    }
    
}


function mayorEdad($fechanac){
    
    //Recojo la fecha actual
    $dia=date("j"); //Día del mes (1-31) actual
    $mes=date("n"); //Mes del año (1-12) actual
    $anno=date("Y"); //Año actual
    
   $annonac=$fechanac[0].$fechanac[1].$fechanac[2].$fechanac[3];
   $mesnac=$fechanac[5].$fechanac[6];
   $dianac=$fechanac[8].$fechanac[9];
    
    if($mesnac>$mes){
        $edad= $anno-$annonac-1;
    } else{
        if($mes==$mesnac AND $dianac>$dia){
            $edad= $anno-$annonac-1;  
        } else{
            $edad= $anno-$annonac;
        }
    }
    
    if ( $edad >= 18 ) {
        return true;
    } else {
        return false;
    }

}


//FUNCIÓN RECOGER DATOS FORMULARIO
function recoge($variable){
	$tmp=(isset($_POST[$variable]))
		? trim(htmlspecialchars($_POST[$variable],ENT_QUOTES,"UTF-8",
		strip_tags($_POST[$variable])))
		:"";
		return $tmp;
}


//CONEXIÓN CON LA BD
include("conexion_ayuntamiento.php");

$conexion_ayuntamiento=mysqli_connect($servername, $username, $password, $db);

if(!$conexion_ayuntamiento){

  die("Conexión Fallida".mysqli_connect_error());

}


//RECOGIDA DE LOS DATOS
$documento=recoge("documento"); 
$codigo=recoge("codigo"); 
$nombre=recoge("nombre");
$apellido=recoge("apellido");   
$segundoapellido=recoge("segundoapellido");
$residencia=recoge("residencia");
$fechanac=recoge("fechanac");
$titulacion=recoge("titulacion");
$email=recoge("email");
$telefono=recoge("telefono");
$interes=$_POST["interes"];
$intereses="";
$otros=recoge("otros");

$error=""; //Mensaje de error


if ( trim($codigo)=="" ) {
    $error=$error."Debe introducir el número del DNI.\\n";
}
if ( trim($nombre)=="" ) {
    $error=$error."Debe introducir el nombre.\\n";
}
if ( trim($apellido)=="" ) {
    $error=$error."Debe introducir el primer apellido.\\n";
}
if ( trim($residencia)=="" ) {
    $error=$error."Debe introducir dónde reside.\\n";
}
if ( trim($email)=="" AND trim($telefono)=="" ) {
    $error=$error."Debe introducir al menos un método de contacto.\\n";
}
if ( empty($interes) ){
     $error=$error."Debe marcar al menos un campo en el que esté interesado.\\n";
}
if ( fechaOK($fechanac) ){
    if ( !mayorEdad($fechanac) ){
        $error=$error."Debe ser mayor de edad para inscribirse al censo de buscadores de empleo.\\n";
    }
}  else{
    $error=$error."Ha introducido una fecha de nacimiento incorrecta.\\n";
}



if ( !empty($error) ) { //Si hay algún error salta el mensjae y vuelve al formulario
    
    echo "<script>alert('$error');history.back();</script>";

} else{    
    
    for ( $i=0 ; $i<count($interes) ; $i++ ) {
        $intereses=$intereses.$interes[$i];
        if ( $i<(count($interes)-1) ){
             $intereses=$intereses.",";
        }
    } 
    
    $insertar_fila="insert into ins_empleo (documento,dni,nombre,pri_apellido,seg_apellido,fecha_nac,residencia,fecha_insc,email,telefono,titulacion,emp_interes,comentario) VALUES ('$documento','$codigo','$nombre','$apellido','$segundoapellido','$fechanac','$residencia',CURDATE(),'$email','$telefono','$titulacion','$intereses','$otros')";
    
    echo "<br><br>$insertar_fila<br><br>";
    
    print "$documento <br>";
    print "$codigo <br>";
    print "$nombre <br>";
    print "$apellido <br>";
    print "$segundoapellido <br>";
    print "$fechanac <br>";
    print "$titulacion <br>";
    print "$residencia <br>";
    print "$email <br>";
    print "$telefono <br>";
    print "$intereses<br>";
    print "$otros <br>";
    
    
    if (mysqli_query($conexion_ayuntamiento,$insertar_fila) ) {
        echo "Se han insertado los datos correctamente.";
        
    } else{
        echo "No se ha hecho la inserción.<br>";
        die("Conexión Fallida".mysqli_connect_error());
    }
    
}





/*


//CONTROL E INSERCION CITA PREVIA ENTIDAD

if ( $persona == "Juridica" AND $documento =="CIF" AND $codigo != "" AND $razon != "" AND ( $email != "" OR $telefono !="" ) ) {
    //Los datos son correctos y están rellenados los obligatorios
    //INSERCION ENTIDAD
    $insertar_fila="INSERT INTO cita_entidad(cif, razon_social, nombre, pri_apellido, seg_apellido, municipio, dpto, email, telefono, motivo, fecha_pet, fecha_cita) VALUES ('$codigo','$razon','$nombre','$apellido','$segundoapellido','$municipio','$departamento','$email','$telefono','$otros',curdate(),'$fechacita')";

}   else {
    //Los datos no son correctos, no se hace la inserción
    
}


//CONTROL E INSERCION CITA PREVIA PERSONA
if ( $persona == "Fisica" AND ($documento =="NIF" OR $documento =="NIE") AND $codigo != "" AND $nombre != "" AND $apellido != "" AND ( $email != "" OR $telefono != "" ) ) {
    //Los datos son correctos y están rellenados los obligatorios
    //INSERCION PERSONA
    $insertar_fila="INSERT INTO cita_persona(dni, nombre, pri_apellido, seg_apellido, municipio, dpto, email, telefono, motivo, fecha_pet, fecha_cita) VALUES ('$codigo','$nombre','$apellido','$segundoapellido','$municipio','$departamento','$email','$telefono','$otros',curdate(),'$fechacita')";
}   else {
    //Los datos no son correctos, no se hacen la inserción
    
}






  if (mysqli_query($conexion_ayuntamiento,$insertar_fila)) {

  //Aquí consulto los datos que le introduzco en un formulario.
      
      
    $result = mysqli_query($conexion_ayuntamiento, "SELECT * FROM cita_entidad "); 

    echo "<table border = '1'>"; 

    print "<tr><td>codigo</td><td>razon_social</td><td>nombre</td><td>pri_apellido</td><td>seg_apellido</td><td>municipio</td><td>dpto</td><td>email</td><td>telefono</td><td>motivo</td><td>fecha_pet</td><td>fecha_cita</td></tr> \n"; 
      
    while ($row = mysqli_fetch_assoc($result)){ 
      
    print "<tr>"; 
        
        print "<td>$row[cif]</td>";
        print "<td>$row[razon_social]</td>";
        print "<td>$row[nombre]</td>";
        print "<td>$row[pri_apellido]</td>";
        print "<td>$row[seg_apellido]</td>";
        print "<td>$row[municipio]</td>";
        print "<td>$row[dpto]</td>";
        print "<td>$row[email]</td>";
        print "<td>$row[telefono]</td>";
        print "<td>$row[motivo]</td>";
        print "<td>$row[fecha_pet]</td>";
        print "<td>$row[fecha_cita]</td>";
        
    print "</tr>"; 
    
    }
      
      
  echo "</table>";
      
  }

*/

?>