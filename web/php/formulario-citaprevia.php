<?php

//FUNCIÓN RECOGER DATOS FORMULARIO
function recoge($variable){
	$tmp=(isset($_POST[$variable]))
		? trim(htmlspecialchars($_POST[$variable],ENT_QUOTES,"UTF-8",
		strip_tags($_POST[$variable])))
		:"";
		return $tmp;
}

//CONEXIÓN CON LA BD
include("conexion_ayuntamiento.php");

$conexion_ayuntamiento=mysqli_connect($servername, $username, $password, $db);

if(!$conexion_ayuntamiento){

  die("Conexión Fallida".mysqli_connect_error());

}


//RECOGIDA DE LOS DATOS
$persona=recoge("persona"); //FISICA O JURIDICA
$documento=recoge("documento"); //TIPO 
$codigo=recoge("codigo"); //NUMERO
$nombre=recoge("nombre");
$apellido=recoge("apellido");
$segundoapellido=recoge("segundoapellido");
$cargo=recoge("cargo");
$razon=recoge("razon");
$municipio=recoge("municipio");
$departamento=recoge("departamento");
$fechacita=recoge("fechacita");
$email=recoge("email");
$telefono=recoge("telefono");
$otros=recoge("otros");


$error=""; //Mensaje de error


if ( trim($residencia)=="" ) {
    $error=$error."Debe introducir dónde reside.\\n";
}
if ( trim($email)=="" AND trim($telefono)=="" ) {
    $error=$error."Debe introducir al menos un método de contacto.\\n";
}
if ( empty($interes) ){
     $error=$error."Debe marcar al menos un campo en el que esté interesado.\\n";
}
if ( fechaOK($fechanac) ){
    if ( !mayorEdad($fechanac) ){
        $error=$error."Debe ser mayor de edad para inscribirse al censo de buscadores de empleo.\\n";
    }
}  else{
    $error=$error."Ha introducido una fecha de nacimiento incorrecta.\\n";
}


// *****************  CONTROL *****************

if ( $persona == "Juridica" ) {
    
    if ( trim($razon)=="" ) {
    $error=$error."Debe introducir el nombre.\\n";
    }
    if ( trim($email)=="" AND trim($telefono)=="" ) {
    $error=$error."Debe introducir al menos un método de contacto.\\n";
    }
    
    $insertar_fila="INSERT INTO cita_entidad(cif, razon_social, nombre, pri_apellido, seg_apellido, municipio, dpto, email, telefono, motivo, fecha_pet, fecha_cita) VALUES ('$codigo','$razon','$nombre','$apellido','$segundoapellido','$municipio','$departamento','$email','$telefono','$otros',curdate(),'$fechacita')";

}


//CONTROL E INSERCION CITA PREVIA PERSONA
if ( $persona == "Fisica" ) {
    if ( trim($nombre)=="" ) {
    $error=$error."Debe introducir el nombre.\\n";
    }
    if ( trim($apellido)=="" ) {
        $error=$error."Debe introducir el primer apellido.\\n";
    }
    if ( trim($email)=="" AND trim($telefono)=="" ) {
    $error=$error."Debe introducir al menos un método de contacto.\\n";
    }
    
    $insertar_fila="INSERT INTO cita_persona(dni, nombre, pri_apellido, seg_apellido, municipio, dpto, email, telefono, motivo, fecha_pet, fecha_cita) VALUES ('$codigo','$nombre','$apellido','$segundoapellido','$municipio','$departamento','$email','$telefono','$otros',curdate(),'$fechacita')";
}



// *************************************************************************************





  if (mysqli_query($conexion_ayuntamiento,$insertar_fila)) {

  //Aquí consulto los datos que le introduzco en un formulario.
      
      
    $result = mysqli_query($conexion_ayuntamiento, "SELECT * FROM cita_entidad "); 

    echo "<table border = '1'>"; 

    print "<tr><td>codigo</td><td>razon_social</td><td>nombre</td><td>pri_apellido</td><td>seg_apellido</td><td>municipio</td><td>dpto</td><td>email</td><td>telefono</td><td>motivo</td><td>fecha_pet</td><td>fecha_cita</td></tr> \n"; 
      
    while ($row = mysqli_fetch_assoc($result)){ 
      
    print "<tr>"; 
        
        print "<td>$row[cif]</td>";
        print "<td>$row[razon_social]</td>";
        print "<td>$row[nombre]</td>";
        print "<td>$row[pri_apellido]</td>";
        print "<td>$row[seg_apellido]</td>";
        print "<td>$row[municipio]</td>";
        print "<td>$row[dpto]</td>";
        print "<td>$row[email]</td>";
        print "<td>$row[telefono]</td>";
        print "<td>$row[motivo]</td>";
        print "<td>$row[fecha_pet]</td>";
        print "<td>$row[fecha_cita]</td>";
        
    print "</tr>"; 
    
    }
      
      
  echo "</table>";
      
  }



?>