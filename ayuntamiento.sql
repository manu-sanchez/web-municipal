-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-05-2019 a las 13:50:47
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ayuntamiento`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cita_entidad`
--

CREATE TABLE `cita_entidad` (
  `cif` char(9) DEFAULT NULL,
  `razon_social` varchar(30) DEFAULT NULL,
  `nombre` varchar(20) NOT NULL,
  `pri_apellido` varchar(20) NOT NULL,
  `seg_apellido` varchar(20) DEFAULT NULL,
  `municipio` varchar(30) DEFAULT NULL,
  `dpto` varchar(50) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `telefono` char(9) DEFAULT NULL,
  `motivo` varchar(300) DEFAULT NULL,
  `fecha_pet` date DEFAULT NULL,
  `fecha_cita` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cita_persona`
--

CREATE TABLE `cita_persona` (
  `dni` char(9) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `pri_apellido` varchar(20) NOT NULL,
  `seg_apellido` varchar(20) DEFAULT NULL,
  `municipio` varchar(30) DEFAULT NULL,
  `dpto` varchar(50) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `telefono` char(9) DEFAULT NULL,
  `motivo` varchar(300) DEFAULT NULL,
  `fecha_pet` date DEFAULT NULL,
  `fecha_cita` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudadanos`
--

CREATE TABLE `ciudadanos` (
  `dni` char(9) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `pri_apellido` varchar(20) NOT NULL,
  `seg_apellido` varchar(20) DEFAULT NULL,
  `direcc` varchar(40) DEFAULT NULL,
  `nacionalidad` varchar(15) DEFAULT NULL,
  `estado_civil` varchar(10) DEFAULT NULL,
  `empadronado` tinyint(1) NOT NULL,
  `sexo` char(1) NOT NULL,
  `fecha_nac` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `concejal`
--

CREATE TABLE `concejal` (
  `dni` char(9) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `pri_apellido` varchar(20) NOT NULL,
  `seg_apellido` varchar(20) DEFAULT NULL,
  `concejalia` varchar(35) DEFAULT NULL,
  `partido` varchar(30) DEFAULT NULL,
  `cargo` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entidad`
--

CREATE TABLE `entidad` (
  `razon_social` varchar(30) DEFAULT NULL,
  `cif` char(9) DEFAULT NULL,
  `direcc` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funcionario_administrativa`
--

CREATE TABLE `funcionario_administrativa` (
  `dni` char(9) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `pri_apellido` varchar(20) NOT NULL,
  `seg_apellido` varchar(20) DEFAULT NULL,
  `cod_empleado` char(50) DEFAULT NULL,
  `dpto` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funcionario_policia`
--

CREATE TABLE `funcionario_policia` (
  `dni` char(9) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `pri_apellido` varchar(20) NOT NULL,
  `seg_apellido` varchar(20) DEFAULT NULL,
  `num_placa` char(6) DEFAULT NULL,
  `cargo` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ins_empleo`
--

CREATE TABLE `ins_empleo` (
  `dni` char(9) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `pri_apellido` varchar(20) NOT NULL,
  `seg_apellido` varchar(20) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `telefono` char(9) DEFAULT NULL,
  `titulacion` varchar(40) DEFAULT NULL,
  `emp_interes` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_sede`
--

CREATE TABLE `usuario_sede` (
  `usuario` varchar(20) NOT NULL,
  `clave` varchar(20) DEFAULT NULL,
  `dni` char(9) NOT NULL,
  `email` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cita_persona`
--
ALTER TABLE `cita_persona`
  ADD PRIMARY KEY (`dni`);

--
-- Indices de la tabla `ciudadanos`
--
ALTER TABLE `ciudadanos`
  ADD PRIMARY KEY (`dni`);

--
-- Indices de la tabla `concejal`
--
ALTER TABLE `concejal`
  ADD PRIMARY KEY (`dni`);

--
-- Indices de la tabla `funcionario_administrativa`
--
ALTER TABLE `funcionario_administrativa`
  ADD PRIMARY KEY (`dni`);

--
-- Indices de la tabla `funcionario_policia`
--
ALTER TABLE `funcionario_policia`
  ADD PRIMARY KEY (`dni`);

--
-- Indices de la tabla `ins_empleo`
--
ALTER TABLE `ins_empleo`
  ADD PRIMARY KEY (`dni`);

--
-- Indices de la tabla `usuario_sede`
--
ALTER TABLE `usuario_sede`
  ADD PRIMARY KEY (`usuario`),
  ADD UNIQUE KEY `dni` (`dni`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
